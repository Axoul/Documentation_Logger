============================================
Documentation sur la convertisseur ADS122U04
============================================

Voici la page qui explique chaque fonction du convertisseur Analogique-Numérique.

Appel de la classe
==================

Pour appeler la classe du convertisseur (avant le main), il faut donc utiliser cet appel de classe :

.. cpp:function:: ADS122U04(uint8_t m_drdy)

``m_drdy`` est le GPIO de fin de conversion qui est le numéro 35 dans notre cas.
Pour résumer, il suffit d'appeler la classe de cette manière en prenant par exemple un alias ``conv`` : ::

  ADS122U04 conv(35);

Dorénavant avant chaque appel de fonction il faudra utiliser cet alias en tant que classe de cette manière : ::

  conv.maFonction();


Fonctions de démarrage
======================

Pour démarrer le convertisseur, il faut d'abord l'alimenter en allumant le régulateur en question : ::

  digitalWrite(regCan, HIGH);

Il est utile de laisser un délai de quelques millisecondes après l'allumage du régulateur pour que la tension soit stable.

Il faut maintenant reset le convertisseur pour le démarrer :

.. cpp:function:: void reset(void)

Puis le l'initialiser par cette fonction :

.. cpp:function:: void begin(void)

Cette fonction configure les entrées et sorties des GPIO et met en place une configuration par défaut :

==========================  ======= ======================
AIN P = AIN0, AIN N = AIN1  PGA_OFF 20 samples par seconde
==========================  ======= ======================


Fonctions de configuration
==========================

Voici la liste des fonctions disponibles pour configurer facilement les registres de l'ADS122U04.

.. cpp:class:: ADS122U04

  .. cpp:function:: void PGA_ON(void)

  Activation de l'amplificateur avec gain programmable (PGA).

  |

  .. cpp:function:: void PGA_OFF(void)

  Désactivation de l'amplificateur avec gain programmable (PGA).

  |

  .. cpp:function:: void setPgaGain(int pgagain)

  Réglage du gain du PGA.

  :param int pgagain: Valeurs possible pour le gain : ``PGA_GAIN_1`` ``PGA_GAIN_2`` ``PGA_GAIN_4`` ``PGA_GAIN_8`` ``PGA_GAIN_16`` ``PGA_GAIN_32`` ``PGA_GAIN_64`` ``PGA_GAIN_128``

  |

  .. cpp:function:: void normalModeON(void)

  Activation du mode normal pour la cadence d'acquisition.

  |

  .. cpp:function:: void turboModeON(void)

  Activation du mode turbo pour la cadence d'acquisition.

  |

  .. cpp:function:: void setDataRate(int datarate)

  Réglage de la cadence d'acquisition. Ces valeurs sont doublées si le mode Turbo est activé.

  :param int datarate: Valeur possible pour la cadence d'acquisition : ``DR_20SPS`` ``DR_45SPS`` ``DR_90SPS`` ``DR_175SPS`` ``DR_330SPS`` ``DR_600SPS`` ``DR_1000SPS``

  |

  .. cpp:function:: void singleShotModeON(void)

  Activation du mode "coup à coup". Nécessite un ``void startSync(void)`` pour chaque acquisition.

  |

  .. cpp:function:: void continuousConversionModeON(void)

  Activation du mode d'acquisition continu.

  |

  .. cpp:function:: void drdyControlON(void)

  Activation de la patte de contrôle de fin de conversion.

  |

  .. cpp:function:: void temperatureSensorON(void)

  Activation du capteur de température.
  Pour le désactiver ``void temperatureSensorOFF(void)``.

  |

  .. cpp:function:: void setInputMultiplexer(int setting)

  Permet le réglage des différentes entrées du convertisseur soit 2 doubles et 4 simples.

  :param int setting: Valeurs possible pour le réglages de entrées : ``AIN0_AIN1_CONFIG`` ``AIN0_AIN2_CONFIG`` ``AIN0_AIN3_CONFIG`` ``AIN1_AIN0_CONFIG`` ``AIN1_AIN2_CONFIG`` ``AIN1_AIN3_CONFIG`` ``AIN2_AIN3_CONFIG`` ``AIN3_AIN2_CONFIG`` ``AIN0_AVSS_CONFIG`` ``AIN1_AVSS_CONFIG`` ``AIN2_AVSS_CONFIG`` ``AIN3_AVSS_CONFIG``


Fonctions de mise en marche/arrêt
=================================

.. cpp:class:: ADS122U04

  .. cpp:function:: void startSync(void)

  Fonction de démarrage de la conversion. A utiliser avant toutes lectures de conversion manuelle.

  |

  .. cpp:function:: void powerDown(void)

  Fonction du mode veille suite à une conversion. Pour réveiller le convertisseur il suffit d'appeler la fonction ``void reset(void)``.


Fonctions de lecture
====================

.. cpp:class:: ADS122U04

  Voici la liste des fonctions disponibles qui permettent la lecture de la conversion.

  .. cpp:function:: uint32_t manualRead(void)

  Fonction de lecture de la valeur brut de la conversion sur 24bits.

  :rtype: uint32_t

  |

  .. cpp:function:: float manualReadVolt(void)

  Fonction de lecture de la valeur convertie en tension. Ne pas oublier de diviser cette valeur par le gain du PGA.

  :rtype: float

  |

  .. cpp:function:: uint32_t autoRead(void)

  Fonction de lecture de la valeur brut de la conversion sur 24bits quand le mode de conversion continue est activée.

  :rtype: uint32_t

  |

  .. cpp:function:: float autoReadVolt(void)

  Fonction de lecture de la valeur convertie en tension quand le mode de conversion continue est activée. Ne pas oublier de diviser cette valeur par le gain du PGA.

  :rtype: float

  |

  .. cpp:function:: float readTemp(void)

  Fonction de lecture de la valeur du capteur de température en degrés Celsius.

  :rtype: float


Exemple de conversion
=====================

.. code-block:: cpp

  ADS122U04 conv(35);

  void setup(){
    digitalWrite(regCan, HIGH);
    can.begin(9600, SERIAL_8N1, 4, 2); //Initialisation UART
    delay(50);
    conv.reset();
    delay(50);
    conv.begin();
    conv.singleShotModeON();
    //Configuration à faire ici
  }

  void loop(){
    ...
    conv.reset();
    conv.startSync();
    delay(70);
    float out = conv.manualReadVolt();
    conv.powerDown();
    ...
  }
