==================================================
Enregistrement des données sur les cartes Micro SD
==================================================

Il y a deux cartes Micro SD donc il faut pouvoir utiliser les mêmes fonctions.
De plus les deux périphériques ont leur port SPI propre donc il est bien aussi de choisir son port.

Appel de la classe
==================

Pour appeler la classe des carte micro SD, il faut déclarer l'objet de cette façon :

.. cpp:function:: carteSd sdX(uint8_t m_ssPin, SPIClass spi)

``m_ssPin`` représente le chip select de la carte SD correspondant et ``spi`` est la classe SPI correspondante au port.

Vu qu'il n'y a que deux cartes SD et donc que deux port SPI voici les deux déclarations possibles : ::

  carteSd sd1(5, VSPI);
  carteSd sd2(15, HSPI);

Donc pour les appels de fonctions il faut procéder comme suit : ::

  sd1.maFonction();
  // Ou
  sd2.maFonction();

Par mesure d'économie d'énergie, que ce soit la première ou la seconde carte SD, à chaque appel de fonction la carte en question est monté puis démonté à la fin de la fonction. Il n'est donc pas nécessaire d'éteindre ou d'allumer les cartes individuellement. Mise à par la mise sous tension du régulateur des micro SD.

Les fonctions des micro sd
==========================


.. cpp:class:: carteSd

  La fonction d'initialisation des micro SD
  =========================================

  .. cpp:function:: int init(const char * title)

  :param const char * title: Correspond à l'entête du fichier CSV avec chaque champ de mesure séparé par une virgule et donc le premier champ est le temps (time) ``"time,...,...,..."``

  :return: le numéro du fichier créé.
  :rtype: int

  |

  .. cpp:function:: void logger(unsigned long timestamp, int n_args, ...)

  :param unsigned long timestamp: premier parametre du fichier CSV : L'époch ou timestamp. Correspond à ``time_t now`` pour avoir le temps au moment de l'écriture sur SD.


  :param int n_args: Le nombre de paramètre suivant qui seront passés.

  :param ...: Vous pouvez faire passer autant de paramètres de type ``float`` que vous voulez.
